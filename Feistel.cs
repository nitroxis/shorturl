﻿namespace ShortUrl
{
	/// <summary>
	/// A class that maps an integer to a different one using a bijective function to achieve pseudo-random IDs.
	/// The algorithm is based on the Feistel cipher.
	/// </summary>
	internal static class Feistel
	{
		/// <summary>
		/// The number of bits in an integer.
		/// 2^Bits is the maximum number of possible IDs.
		/// Default is 18 bits which allows for 3 base64 characters (A total of 262144 IDs)
		/// In case you need more, increase this.
		/// </summary>
		public const int Bits = 18;

		/// <summary>
		/// The maximum possible value.
		/// </summary>
		public const int Maximum = 1 << Bits;

		private const int halfBits = Bits / 2;
		private const int lowMask = (1 << halfBits) - 1;
		private const int rounds = 10;

		/// <summary>
		/// The round function.
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		private static int f(int n)
		{
			// this can be changed to generate a completely different set.
			// The low mask must not be removed.
			return (((n ^ 1337) + 42) << 1) & lowMask;
		}

		/// <summary>
		/// Permutes 'n' using the Feistel cipher.
		/// To reverse the permutation, simply permute the number again (i.e. Permute(Permute(n)) == n).
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		public static int Permute(int n)
		{
			int high = n >> halfBits;
			int low = n & lowMask;

			for (int i = 0; i < rounds; i++)
			{
				high = high ^ f(low);

				int temp = high;
				high = low;
				low = temp;
			}

			return high | (low << halfBits);
		}
	}
}
