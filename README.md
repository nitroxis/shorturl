ShortURL
========
A very simple SCGI-based short URL service written in C#.
It handles SCGI requests (tested with nginx' SCGI module) and responds with "301 Permanently Moved" to redirect the user to the long URL.
New URLs can be added by sending a GET request to `/add-url?yourURLhere`. 
The response is a simple JSON string like `{"id":"aBc"}`, or `{"error":{"message": "Invalid URL."}}`.
All other URLs will be treated as short-URL ID. 

URLs are stored in a simple text file, one URL per line.
The shortened ID is the line number in that database, "encrypted" using a simple Feistel cipher to make it look more random and encoded using base64 with a web-friendly table.
By default, a long URL is mapped to a 3-character ID with 64 possible characters (resulting in a total of 64*64*64, or 262144 possible IDs).
To increase the number of possible IDs one needs the change the number of bits in Feistel.cs. 

Usage
-----
	shorturl <ip endpoint> <database>

For example:

	shorturl 127.0.0.1:59001 urls.txt

Mono:

	mono shorturl.exe 127.0.0.1:59001 urls.txt

Example nginx configuration
---------------------------
	location ~ "^\/(([A-Za-z0-9_-]{3})|add-url)$"
	{
		include scgi_params;

		scgi_intercept_errors on;
		scgi_pass localhost:59001;
	}

systemd unit
------------
	[Unit]
	Description=Short URL service
	After=network.target
	Wants=nginx.service

	[Service]
	User=http
	Group=http
	ExecStart=/usr/bin/mono -O=all /home/http/shorturl.exe 127.0.0.1:59001 /home/http/urls.txt

	[Install]
	WantedBy=multi-user.target


License
-------
Copyright (C) 2015 nitroxis

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

