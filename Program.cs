﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace ShortUrl
{
	public static class Program
	{
		private static bool run;
		private static StringSet urls;

		public static int Main(string[] args)
		{
			if (args.Length < 2)
			{
				Console.WriteLine("Usage:");
				Console.WriteLine("shorturl <ip endpoint> <database>");
				return 1;
			}

			// parse command line arguments and load URL database.
			IPEndPoint ep = parseIPEndPoint(args[0], 9000);
			urls = new StringSet(args[1]);
			
			// create TCP listener.
			TcpListener listener = new TcpListener(ep);
			listener.Start();

			run = true;
			Console.CancelKeyPress += (sender, e) => { run = false; };

			while (run)
			{
				// accept TCP clients.
				using (TcpClient client = listener.AcceptTcpClient())
				using (Stream stream = client.GetStream())
				{
					try
					{
						handleRequest(stream);
					}
					catch (Exception ex)
					{
						Console.WriteLine("Failed to handle request: {0}", ex);
					}
				}
			}

			listener.Stop();

			return 0;
		}
		
		private static void handleRequest(Stream stream)
		{
			const string base64 = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-";

			NameValueCollection headers = Scgi.ReadHeaders(stream);
			
			using (StreamWriter writer = new StreamWriter(stream))
			{
				string documentUri = headers["DOCUMENT_URI"];
				
				if (documentUri == "/add-url")
				{
					// get URL.
					string url = Uri.UnescapeDataString(headers["QUERY_STRING"]);
					if (!Uri.IsWellFormedUriString(url, UriKind.Absolute) || url.Length >= 8192)
					{
						Console.WriteLine("Invalid URL {0}", url);
						
						writer.WriteLine("Status: 400 Bad Request");
						writer.WriteLine("Content-Type: application/json");
						writer.WriteLine();
						writer.WriteLine("{\"error\":{\"message\":\"Invalid URL.\"}}");
						return;
					}

					// add URL and randomize its index.
					int id = Feistel.Permute(urls.Add(url));
					
					// base64.
					string str = "";
					while (id > 0 || str.Length < 3)
					{
						str = base64[id & 63] + str;
						id >>= 6;
					}

					// respond.
					writer.WriteLine("Status: 200 OK");
					writer.WriteLine("Content-Type: application/json");
					writer.WriteLine();
					writer.WriteLine("{\"id\":\"" + str + "\"}");

					Console.WriteLine("Shortened {0} -> {1}", url, str);
				}
				else
				{
					// un-base64 the ID.
					int id = 0;
					for (int i = 1; i < documentUri.Length; i++)
						id = (id << 6) | base64.IndexOf(documentUri[i]);

					// un-randomize the ID.
					id = Feistel.Permute(id);

					// check bounds.
					if (id < 0 || id >= urls.Count)
					{
						// 404.
						writer.WriteLine("Status: 404 Not Found");
						writer.WriteLine();
					}
					else
					{
						// respond with redirection.
						string url = urls[id];
						writer.WriteLine("Status: 301 Moved Permanently");
						writer.WriteLine("Location: " + url);
						writer.WriteLine();
					}
				}
			}
		}

		private static IPEndPoint parseIPEndPoint(string endPoint, int defaultPort)
		{
			string[] ep = endPoint.Split(':');
			if (ep.Length < 2)
				return new IPEndPoint(IPAddress.Parse(endPoint), defaultPort);
			
			return new IPEndPoint(IPAddress.Parse(ep.Length > 2 ? string.Join(":", ep, 0, ep.Length - 1) : ep[0]), int.Parse(ep[ep.Length - 1], NumberStyles.None, CultureInfo.InvariantCulture));
		}
	}
}

