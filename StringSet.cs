﻿using System;
using System.IO;
using System.Text;

namespace ShortUrl
{
	/// <summary>
	/// Represents a set of strings which indexes the lines of a simple text file.
	/// </summary>
	internal sealed class StringSet
	{
		#region Nested Types

		/// <summary>
		/// Represents one string (or line in the text file) with an offset, length and CRC32 hash.
		/// </summary>
		private struct Line
		{
			public int Position;
			public int Length;
			public uint Hash;
		}

		#endregion

		#region Fields

		private static readonly uint[] crcTable;
		
		private readonly Stream textFile;

		private Line[] index;
		private int count;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the line at the specified index.
		/// </summary>
		/// <param name="i"></param>
		/// <returns></returns>
		public string this[int i]
		{
			get
			{
				// get line and seek to position, then read n bytes and decode with UTF8.
				Line line = this.index[i];
				byte[] buffer = new byte[line.Length];
				//lock (this.textFile)
				{
					this.textFile.Position = line.Position;
					this.textFile.Read(buffer, 0, line.Length);
				}
				return Encoding.UTF8.GetString(buffer, 0, line.Length);
			}
		}

		/// <summary>
		/// Gets the total number of lines.
		/// </summary>
		public int Count
		{
			get { return this.count; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Static constructor.
		/// </summary>
		static StringSet()
		{
			// initialize CRC table.
			crcTable = new uint[256];
			const uint polynomial = 0xEDB88320u;
			for (uint i = 0; i < 256; i++)
			{
				uint entry = i;
				for (var j = 0; j < 8; j++)
					if ((entry & 1) == 1)
						entry = (entry >> 1) ^ polynomial;
					else
						entry = entry >> 1;
				crcTable[i] = entry;
			}
		}

		/// <summary>
		/// Creates a new string set from the specified file.
		/// </summary>
		/// <param name="file"></param>
		public StringSet(string file)
		{
			this.index = new Line[64];

			this.textFile = new FileStream(file, FileMode.OpenOrCreate, FileAccess.ReadWrite);
			this.textFile.Position = 0;

			Line line;
			line.Position = 0;
			line.Length = 0;
			line.Hash = 0u;

			// build index.
			while (true)
			{
				int c = this.textFile.ReadByte();

				// end of file.
				if (c == -1)
				{
					if (line.Length > 0)
						this.addLine(line);
						
					break;
				}
			
				// end of line.
				if (c == 10)
				{
					this.addLine(line);

					line.Position = (int)this.textFile.Position;
					line.Length = 0;
					line.Hash = 0u;
					continue;
				}

				// hash.
				line.Hash = crcTable[(c ^ line.Hash) & 0xFF] ^ (line.Hash >> 8);
				line.Length++;
			}
		}

		#endregion

		#region Methods

		private void addLine(Line line)
		{
			this.index[this.count++] = line;
			if (this.count >= this.index.Length)
			{
				Array.Resize(ref this.index, this.count + 64);
				Console.WriteLine("Index size: {0} lines ({1} KiB).", this.count, this.count * 12 / 1024);
			}
		}

		/// <summary>
		/// Adds a new line.
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public int Add(string str)
		{
			// get bytes.
			Line line;
			byte[] buffer = Encoding.UTF8.GetBytes(str);
			line.Length = buffer.Length;

			// compute hash.
			line.Hash = 0;
			for (int i = 0; i < line.Length; ++i)
				line.Hash = crcTable[(buffer[i] ^ line.Hash) & 0xFF] ^ (line.Hash >> 8);

			// check if it exists already.
			// lock (this.index)
			{
				for (int i = 0; i < this.count; i++)
				{
					if (this.index[i].Hash == line.Hash && this[i] == str)
						return i;
				}
			}
		
			// go to end of file.
			// lock (this.textFile)
			{
				this.textFile.Seek(0, SeekOrigin.End);
				line.Position = (int)this.textFile.Position;

				// write line.
				this.textFile.Write(buffer, 0, buffer.Length);
				this.textFile.WriteByte(10);
				this.textFile.Flush();
			}

			// add to index.
			// lock (this.index)
			{
				this.addLine(line);
				return this.count - 1;
			}
		}

		#endregion
	}
}
