﻿using System.Collections.Specialized;
using System.IO;
using System.Text;

namespace ShortUrl
{
	/// <summary>
	/// Simple, minimalistic SCGI implementation.
	/// </summary>
	internal static class Scgi
	{
		private static int readLength(Stream stream)
		{
			int length = 0;

			while (true)
			{
				int c = stream.ReadByte();
				if (c < 0)
					throw new EndOfStreamException();

				if (c == ':')
					break;

				if (c < '0' || c > '9')
					throw new InvalidDataException();

				length = (length * 10) + (c - '0');
			}

			return length;
		}

		private static string readNetString(Stream stream)
		{
			int length = readLength(stream);

			byte[] buffer = new byte[length];
			int offset = 0;

			while (offset < length)
			{
				int numBytes = stream.Read(buffer, offset, length - offset);
				if (numBytes == 0)
					throw new EndOfStreamException();

				offset += numBytes;
			}

			if (stream.ReadByte() != ',')
				throw new InvalidDataException();

			return Encoding.ASCII.GetString(buffer);
		}

		private static NameValueCollection parseHeaders(string headers)
		{
			string[] split = headers.Split('\0');
			NameValueCollection dict = new NameValueCollection();

			for (int i = 0; i < (split.Length & ~1); i += 2)
				dict[split[i]] = split[i + 1];

			return dict;
		}

		public static NameValueCollection ReadHeaders(Stream stream)
		{
			return parseHeaders(readNetString(stream));
		}
	}
}
